import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col'
import InputGroup from 'react-bootstrap/InputGroup'
import Button from 'react-bootstrap/Button';
import React, { useState } from 'react';
import Row from 'react-bootstrap/Row'
import { useHistory } from 'react-router-dom';

function BookingForm(props) {
    const [validated, setValidated] = useState(false);
    const [disabled, setDisabled] = useState(false);

    const history = useHistory();
    const handleSubmit = (event) => {
        event.preventDefault();

      const form = event.currentTarget;
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }else{
        setDisabled(false)
        props.handleBookEvent()        
      }
      setValidated(true);
    };
  
    const handleClick = () => {
        history.push("/");
    }

    return (
      <Form noValidate validated={validated} onSubmit={handleSubmit}>
          <Form.Group>
            <Form.Control
              type="text"
              placeholder="Name"
              required
            />
            <Form.Control.Feedback type="invalid">Please Enter Name</Form.Control.Feedback>
          </Form.Group> 
            <br/>
          <Form.Group>
            <InputGroup>

              <Form.Control
                type="email"
                placeholder="Email"
                aria-describedby="inputGroupPrepend"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please enter valid email.
              </Form.Control.Feedback>
            </InputGroup>
          </Form.Group>
          <br/>

          <Form.Group>
            <Form.Control
              type="text"
              placeholder="Phone"
              required

            />
            <Form.Control.Feedback type="invalid">Please Enter Phone Number</Form.Control.Feedback>
          </Form.Group> 


          <Form.Group controlId="exampleForm.SelectCustom">
            <Form.Label>No of seats</Form.Label>
            <Form.Control as="select" custom>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            </Form.Control>
         </Form.Group>

        <Row>
            <Col>
                <Button disabled={disabled} type="submit">Submit</Button>

            </Col>
            <Col>
             <Button disabled={disabled} onClick={()=> handleClick()} >Cancel</Button>

            </Col>
        </Row>

      </Form>
    );
  }
  

  export default BookingForm;
