import React from 'react';
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import Row from 'react-bootstrap/Row'
import BookingForm from './BookingForm'

class EventDetails extends React.Component {
 
  constructor(props){  
    super(props)
    this.state = {  
         eventDetails: this.props.location.eventDetails,
         isEventBooked: false

      }  
      this.handleBookEvent = this.handleBookEvent.bind(this)
  }  

  handleBookEvent(){
        this.setState({isEventBooked: true})      
  }

  render() {
      if(this.props.location.eventDetails){
        return (
            <div className="App">
              <br/>
                

                {this.state.isEventBooked ? (
                        <p>Event Booking Succesfully done,Your booking no #{Math.floor(100000 + Math.random() * 900000)}</p>
                    ) : (
                        <p>Event Details</p>
                    )}


                 <Card  md={12}>
                    <Card.Body>
                    <Row>
                    <Col xs={6}>
                    <Card.Img className="imageViewDetailsPage" variant="top" src={this.props.location.eventDetails.img} />

                      <Card.Title>{this.props.location.eventDetails.name}</Card.Title>
                      <Card.Text>
                        Seats Available: {this.props.location.eventDetails.slots}
                      </Card.Text>
                    </Col>
                    <Col xs={6}>
                       
                     <BookingForm handleBookEvent={this.handleBookEvent} />

                    </Col>                    
                    </Row>

                    </Card.Body>
                  </Card>
    
    
          </div>
        );
      }else{
          return <div>
              <p>Please redirect to Home page </p>
          </div>
      }

  }
}

export default EventDetails;
