import React from 'react';
import './Events.css';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import Row from 'react-bootstrap/Row'

import Button from 'react-bootstrap/Button';

class Home extends React.Component {
 
  constructor(props){  
    super(props)
    this.state = {  
         eventList: []  
      }  
  }  

 
componentDidMount(){

  fetch("http://localhost:5000/eventlist")
  .then(response => response.json())
  .then(data => 
    this.setState({eventList: data})
  );
  
}

  handleChange(event){
        var params = {"name": event.target.value }        
        var url = new URL("http://localhost:5000/eventlist")
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

        fetch(url)
        .then(response => response.json())
        .then(data => 
          this.setState({eventList: data})
        );

  }

  handleButtonClick(event){
    this.props.history.push({
      pathname: '/eventdetails',
      eventDetails: event
    })

  }

  render() {
    return (
        <div className="App">
          <br/>
        <Form>
          <Form.Row>
            <Col xs={5}>
              <Form.Control onChange={this.handleChange.bind(this)} placeholder="SEARCH EVENTS" />
            </Col>
          </Form.Row>
        </Form>
        <br/><br/>
        <Row>
          {this.state.eventList.map(event => (
            <Card  md={4} style={{ width: '18rem' }}>
                <Card.Img className="imageClass" variant="top" src={event.img} />
                <Card.Body>
                  <Card.Title>{event.name}</Card.Title>
                  <Card.Text>
                    Seats Available: {event.slots}
                  </Card.Text>

                  {event.slots !== "0" ? (
                    <Button onClick={() => this.handleButtonClick(event)} variant="primary">Book Now</Button>
                  ) : (
                    <Button variant="warning">Sold out</Button>
                    )}

                </Card.Body>
              </Card>

          ))}
        </Row>


      </div>
    );
  }
}

export default Home;
